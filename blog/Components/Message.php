<?php

namespace Blog\Components;


class Message
{
  private $success;
  private $message;
  function __construct(bool $success, string $message)
  {
    $this->success = $success;
    $this->message = $message;
  }
  function display()
  {
    $class = ($this->success ? "success" : "fail");
    return <<<EOF
<div class="$class">
	$this->message
</div>
EOF;
  }
}
