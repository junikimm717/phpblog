<?php

namespace Blog\Components;

use Blog\Components\NavbarItem;

class Navbar
{
  function display()
  {
    $items = array(
      new NavbarItem("Posts", "/posts.php")
    );
    if (isset($_SESSION["user"])) {
      $username = htmlspecialchars($_SESSION["user"]);
      $items[] = new NavbarItem("Log Out", "/logout.php");
      $items[] = new NavbarItem("$username");
    } else {
      $items[] = new NavbarItem("Log In", "/login.php");
      $items[] = new NavbarItem("Register", "/register.php");
    }

    $res = "";
    foreach ($items as &$i) {
      $res .= $i->display();
    }
    return <<<EOF
    <div class="navbar">
      <a href="/index.php" class="navbartitle">
        Juni's Blog
      </a>
      $res
    </div>
    EOF;
  }
}
