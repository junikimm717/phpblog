<?php
namespace Blog\Components;

class NavbarItem {
	public $title;
	public $link;
	function __construct($title, $link=null) {
		$this->title = $title;
		$this->link = $link;
	}
	function display() {
		if (isset($this->link)) {
			return <<<EOF
			<a href="$this->link" class="navbaritem navbarlink">$this->title</a>
			EOF;
		} else {
			return <<<EOF
			<div class="navbaritem">$this->title</div>
			EOF;
		}
	}
}

?>
