<?php

namespace Blog\Components;

class Headers
{
  public $title;
  function __construct($title)
  {
    $this->title = $title;
  }
  function display()
  {
    return <<<EOF
			<head>
				<title> $this->title </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link rel="stylesheet" href="/static/index.css">
			</head>
EOF;
  }
}
