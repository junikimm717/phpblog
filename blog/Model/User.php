<?php
namespace Blog\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="User")
 */
class User {
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue
  */
  private $id;

  /**
  @ORM\Column(type="string")
  */
  private $username;

  /**
  @ORM\Column(type="string")
  */
  private $password;

  public function authenticate(string $password) {
    return password_verify($password, $this->password);
  }

  public function username() {
    return $this->username;
  }

  public function setUsername(string $username) {
    $this->username = $username;
  }

  public function setPassword (string $password) {
    $this->password = password_hash($password, PASSWORD_DEFAULT);
  }
}
