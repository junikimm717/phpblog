<?php
namespace Blog\Model;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="Blog")
 */
class Blog {
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue
  */
  private $id;

  /**
  @ORM\Column(type="string")
  */
  private $title;

  /**
  @ORM\Column(type="text")
  */
  private $text;

  public function getTitle() {
	return $this->title;
  }

  public function getText() {
	return $this->text;
  }

  public function setTitle($title) {
	$this->title = $title;
  }

  public function setText($text) {
	$this->text = $text;
  }
}
