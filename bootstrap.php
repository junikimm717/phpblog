<?php
require_once "vendor/autoload.php";

// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

//if (file_exists(".env")) {
//$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
//$dotenv->load();
//}

$user = isset($_ENV["DB_USER"]) ? $_ENV["DB_USER"] : $_SERVER["DB_USER"];
$password = isset($_ENV["DB_PASSWORD"]) ? $_ENV["DB_PASSWORD"] : $_SERVER["DB_PASSWORD"];
$db = isset($_ENV["DB_HOST"]) ? $_ENV["DB_HOST"] : $_SERVER["DB_HOST"];

#echo $user, $password, $db;

#echo "new version being used.";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/blog/Model"), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

#mysqli_connect($db, $user, $user, $password);
mysqli_connect($db, $user, $password, $user, 3306);

$connectionParams = array(
  'dbname' => $user,
  'user' => $user,
  'password' => $password,
  'host' => 'mysql',
  'driver' => 'mysqli',
  'port' => 3306
);
$conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);


// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
