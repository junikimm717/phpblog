#!/bin/sh

while ! mysqladmin ping -h"$DB_HOST" --silent; do
    echo "mysql not up"
    sleep 1
done

./vendor/bin/doctrine orm:schema-tool:drop --force
./vendor/bin/doctrine orm:schema-tool:create
echo "apache2 being run...";
apache2ctl -D FOREGROUND
