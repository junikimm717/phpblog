<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../bootstrap.php';

use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
use Blog\Components\Message;
use Blog\Model\User;

#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

$bar = new Navbar();
$head = new Headers("Register");
$message = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (!isset($_POST["password"]) || strlen($_POST["password"]) < 10) {
    $message = new Message(false, "Password must have at least 10 characters.");
  } else if (
    !isset($_POST["username"]) || strlen($_POST["username"]) < 5
    || !ctype_alnum($_POST["username"])
  ) {
    $message = new Message(false, "Username must have at least 5 characters and be alphanumeric.");
  } else {
    $qb = $entityManager->createQueryBuilder();
    $qb->select('u')->from('Blog\Model\User', 'u')->where('u.username = ?1');
    $qb->setParameter(1, $_POST['username']);
    if ($qb->getQuery()->getOneOrNullResult() !== null) {
      $message = new Message(false, "Username already present.");
    } else {
      $user = new User();
      $user->setUsername(htmlspecialchars($_POST["username"]));
      $user->setPassword($_POST["password"]);
      $entityManager->persist($user);
      $entityManager->flush();
      $_SESSION['user'] = $_POST['username'];
      header("Location: /");
    }
  }
}
?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>
  <div class="login">
    <h1> Register </h1>
    <form method="POST" action="/register.php">
      <label for="username">Username</label>
      <input type="text" name="username" />
      <br><br>
      <label for="password"> Password </label>
      <input type="password" name="password" />
      <br><br>
      <input type="submit" value="Register">
    </form>
  </div>
  <?php if (isset($message)) echo $message->display(); ?>
</body>

</html>
