<?php
require_once __DIR__ . '/../../vendor/autoload.php';
session_start();

use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../..');
$dotenv->load();

$bar = new Navbar();
$head = new Headers("403 Forbidden");

?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>
<body>
	<?php echo $bar->display();?>
	<h1> 403 Forbidden </h1>
</body>
</html>
