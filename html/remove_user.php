<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../bootstrap.php';

#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

if (!isset($_SESSION['user']) || $_SESSION['user'] !== $_ENV['ADMIN_USERNAME']) {
  header('Location: /errors/403.php');
}

if (!isset($_GET['user'])) {
  header('Location: /errors/400.php');
}

$qb = $entityManager->createQueryBuilder();
$qb->delete('Blog\Model\User', 'u')->where('u.username = ?1');
$qb->setParameter(1, $_GET['user']);
$qb->getQuery()->getResult();

header('Location: /admin.php');
