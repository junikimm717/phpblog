<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../bootstrap.php';


use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
use Blog\Components\Message;
use Blog\Model\Blog;
#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

if (!isset($_SESSION['user']) || $_SESSION['user'] !== $_ENV['ADMIN_USERNAME']) {
  header('Location: /errors/403.php');
}

$bar = new Navbar();
$head = new Headers("New Post");

$message = null;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (!isset($_POST['title']) || preg_match("/^\s*$/", $_POST['title'])) {
    $message = new Message(false, "Title cannot be blank");
  } else {
    $b = new Blog();
    $b->setTitle($_POST['title']);
    $b->setText(isset($_POST['text']) ? $_POST['text'] : " ");
    $entityManager->persist($b);
    $entityManager->flush();
    header('Location: /posts.php');
  }
}

?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>
  <h1> New Post </h1>
  <div class="row">
    <form action="/create_blog.php" method="POST">
      <label for="title">
        <h3>Title</h3>
      </label>
      <input type="text" name="title"></input>
      <label for="text">
        <h3> Text </h3>
      </label>
      <textarea name="text" rows=50 cols=35 id="markdown_text"></textarea>
      <br><br>
      <input type="submit" value="Create New Entry">
    </form>
    <div id="markdown_preview"></div>
  </div>
</body>

<!-- jquery and marked -->
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
<script src="/static/editor.js"></script>

</html>
