<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once "../bootstrap.php";


use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
use Blog\Components\Message;

#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

$bar = new Navbar();

$head = new Headers("Juni's Blog");

// Authentication
$message = null;
if ($_SERVER['REQUEST_METHOD'] == "POST") {
  $qb = $entityManager->createQueryBuilder();
  $qb->select('u')
    ->from('Blog\Model\User', 'u')
    ->where('u.username = ?1')
    ->setParameter(1, $_POST['username']);
  $u = $qb->getQuery()->getOneOrNullResult();
  if ($u === null) {
    $message = new Message(false, "Cannot Authenticate. No such user found");
  } else {
    if ($u->authenticate($_POST['password'])) {
      $_SESSION["user"] = $u->username();
      header('Location: /index.php');
    } else {
      $message = new Message(false, "Cannot Authenticate.");
    }
  }
}
?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>
  <div class="login">
    <h1> Log in </h1>
    <form method="POST" action="/login.php">
      <label for="username">Username</label>
      <input type="text" name="username" />
      <br><br>
      <label for="password"> Password </label>
      <input type="password" name="password" />
      <br><br>
      <input type="submit" value="Authenticate">
    </form>
  </div>
  <?php
  if (isset($message)) {
    echo $message->display();
  }
  ?>
</body>

</html>
