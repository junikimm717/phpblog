<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../bootstrap.php';

#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

if (!isset($_SESSION['user']) || $_SESSION['user'] !== $_ENV['ADMIN_USERNAME']) {
  header('Location: /errors/403.php');
}

if (!isset($_GET['id'])) {
  header('Location: /errors/400.php');
}

$qb = $entityManager->createQueryBuilder();
$qb->delete('Blog\Model\Blog', 'b')->where('b.id = ?1');
$qb->setParameter(1, $_GET['id']);
$qb->getQuery()->getResult();

header('Location: /posts.php');
