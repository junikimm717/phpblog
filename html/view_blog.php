<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../bootstrap.php';

use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
use Blog\Components\Message;
use Blog\Model\Blog;
#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

$parser = new \cebe\markdown\GithubMarkdown();

if (!isset($_GET['id'])) {
  header('Location: /errors/404.php');
}

$bar = new Navbar();
$head = new Headers("Edit Post");

$message = null;
$id = $_GET['id'];

$qb = $entityManager->createQueryBuilder();
$qb->select('b')
  ->from('Blog\Model\Blog', 'b')
  ->where('b.id =?1')
  ->setParameter(1, $_GET['id']);

$blog = $qb->getQuery()->getOneOrNullResult();
if ($blog === null) {
  header('Location: /errors/404.php');
  die();
}

?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>
  <h1> <?php echo $blog->getTitle(); ?> </h1>
  <div class="blogpage">
    <?php echo $parser->parse($blog->getText()); ?>
  </div>
</body>

</html>
