<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once '../bootstrap.php';
session_start();

use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

if ($_SESSION['user'] !== $_ENV['ADMIN_USERNAME']) {
  header('Location: /errors/403.php');
}

$bar = new Navbar();
$head = new Headers("Juni's Blog");

// query all posts
$qb = $entityManager->createQueryBuilder();
$users = $qb
  ->select('u')
  ->from('Blog\Model\User', 'u')
  ->setMaxResults(50)
  ->getQuery()
  ->getArrayResult();

?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>
  <h1> All Users </h1>
  <div>
    <?php
    foreach ($users as $u) {
      $user = htmlspecialchars($u['username']);
      $getparam = urlencode($user);
      echo <<<EOF
<div class='usersbox'>
	$user
	<a class='deletebutton' href="/remove_user.php?user=$getparam">
		Delete $user
	</a>
</div>
EOF;
    }
    ?>
  </div>
</body>

</html>
