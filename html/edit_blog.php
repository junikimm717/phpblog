<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../bootstrap.php';


use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
use Blog\Components\Message;
use Blog\Model\Blog;
#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

if (!isset($_SESSION['user']) || $_SESSION['user'] !== $_ENV['ADMIN_USERNAME']) {
  header('Location: /errors/403.php');
}

if (!isset($_GET['id'])) {
  header('Location: /errors/404.php');
}

$bar = new Navbar();
$head = new Headers("Edit Post");

$message = null;
$id = $_GET['id'];

$qb = $entityManager->createQueryBuilder();
$qb->select('b')
  ->from('Blog\Model\Blog', 'b')
  ->where('b.id =?1')
  ->setParameter(1, $_GET['id']);

$blog = $qb->getQuery()->getOneOrNullResult();
if ($blog === null) {
  header('Location: /errors/404.php');
  die();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (!isset($_POST['title']) || preg_match("/^\s*$/", $_POST['title'])) {
    $message = new Message(false, "Title cannot be blank");
  } else {
    // update blog entry
    $entityManager->createQueryBuilder()
      ->update('Blog\Model\Blog', 'u')
      ->set('u.title', ':title')
      ->set('u.text', ':text')
      ->where('u.id = :id')
      ->setParameter('title', $_POST['title'])
      ->setParameter('text', $_POST['text'])
      ->setParameter('id', $_GET['id'])
      ->getQuery()
      ->execute();
    header('Location: /posts.php');
    die();
  }
}

?>
<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>
  <h1> New Post </h1>
  <div class="row">
    <form action="/edit_blog.php?id=<?php echo $id ?>" method="POST">
      <label for="title">
        <h3>Title</h3>
      </label>
      <input type="text" name="title" value='<?php echo $blog->getTitle() ?>'></input>
      <label for="text">
        <h3> Text </h3>
      </label>
      <textarea name="text" rows=50 cols=35 id="markdown_text"><?php echo $blog->getText() ?></textarea>
      <br><br>
      <input type="submit" value="Save Changes">
    </form>
    <div id="markdown_preview"></div>
  </div>
</body>

<!-- jquery and marked -->
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
<script src="/static/editor.js"></script>

</html>
