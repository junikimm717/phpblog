<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';
require_once '../bootstrap.php';

use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
use Blog\Model\Blog;
#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

$bar = new Navbar();
$head = new Headers("Juni's Blog");

// query all posts
$qb = $entityManager->createQueryBuilder();
$posts = $qb->select('u')->from('Blog\Model\Blog', 'u')->getQuery()->getArrayResult();
$isadmin = isset($_SESSION['user']) && $_SESSION['user'] === $_ENV['ADMIN_USERNAME'];

function editButton($post)
{
  $id = urlencode($post['id']);
  return <<<EOF
	<a class="adminbutton" href="/edit_blog.php?id=$id">Edit</a>
	EOF;
}

function deleteButton($post)
{
  $id = urlencode($post['id']);
  return <<<EOF
	<a class="adminbutton" href="/delete_blog.php?id=$id">Delete</a>
	EOF;
}


?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>

  <?php
  if (count($posts) == 0) {
    echo <<<EOF
	<h1> No Posts Uploaded </h1>
	EOF;
  } else {
    echo <<<EOF
	<h1> All Posts </h1>
	EOF;
  }
  ?>
  <div>
    <?php
    if ($isadmin) {
      echo <<<EOF
<a href="/create_blog.php" class="adminbutton">+</a>
EOF;
    }
    ?>
    <ul style="margin-top: 40px;">
      <?php
      foreach ($posts as &$post) {
        $title = htmlspecialchars($post['title']);
        $link = "/view_blog.php?id=" . $post['id'];
        $posttitle = <<<EOF
		<a class="blogtitle" href="$link">
			$title
		</a>
		EOF;
        if ($isadmin) {
          echo "<div class='titlebox'> $posttitle"
            . editButton($post)
            . deleteButton($post) . "</div>";
        } else {
          echo "<div class='titlebox'> $posttitle </div>";
        }
      }
      ?>
    </ul>
  </div>
</body>

</html>
