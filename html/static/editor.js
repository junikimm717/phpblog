const update = () => {
  $("#markdown_preview").html(marked.parse($("#markdown_text").val()));
};

update();

$("#markdown_text").on("input", update);
