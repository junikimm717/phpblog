<?php
require_once __DIR__ . '/../vendor/autoload.php';
session_start();

use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

$bar = new Navbar();
$head = new Headers("Juni's Blog");

?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>
<body>
	<?php echo $bar->display();?>
	<h1> Greetings! </h1>
	<h2> This is Juni's Blog, where he may or may not talk about random stuff.</h2>
</body>
</html>
