<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';

use Blog\Components\Navbar;
use Blog\Components\NavbarItem;
use Blog\Components\Headers;
#$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
#$dotenv->load();

$bar = new Navbar();
$head = new Headers("Juni's Blog");

?>

<!DOCTYPE html>
<html>
<?php echo $head->display(); ?>

<body>
  <?php echo $bar->display(); ?>
  <h1> Greetings! </h1>
  <p>
    This is the blog of Juni Kim, a sophomore at Stanford Online High School.
  </p>
  <h2> Contact </h2>
  <ul>
    <li><a href="https://junickim.me" target="new"> Personal Website </a></li>
    <li><a href="https://github.com/junikimm717" target="new"> Github </a></li>
    <li><a href="https://gitlab.com/junikimm717" target="new"> Gitlab </a></li>
  </ul>
</body>

</html>
