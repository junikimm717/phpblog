FROM php:8.0-apache
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt update -y
RUN apt-get install -y wget zip unzip mariadb-client
RUN docker-php-ext-install mysqli
RUN docker-php-ext-enable mysqli

# install composer
RUN mkdir -p /composer
WORKDIR /composer
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet
RUN mv composer.phar /usr/bin/composer

COPY ./entrypoint.sh /
COPY . /var/www

WORKDIR /var/www
RUN composer install

ENTRYPOINT ["/entrypoint.sh"]
